package com.fisherpavel.redditclone.repositories;

import com.fisherpavel.redditclone.entities.Post;
import com.fisherpavel.redditclone.entities.Subreddit;
import com.fisherpavel.redditclone.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Integer> {
    List<Post> findAllBySubreddit(Subreddit subreddit);

    List<Post> findByUser(User user);
}
