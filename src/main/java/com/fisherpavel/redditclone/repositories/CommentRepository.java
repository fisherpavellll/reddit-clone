package com.fisherpavel.redditclone.repositories;

import com.fisherpavel.redditclone.entities.Comment;
import com.fisherpavel.redditclone.entities.Post;
import com.fisherpavel.redditclone.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {

    List<Comment> findByPost(Post post);

    List<Comment> findAllByUser(User user);
}
