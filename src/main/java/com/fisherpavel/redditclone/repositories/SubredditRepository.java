package com.fisherpavel.redditclone.repositories;

import com.fisherpavel.redditclone.entities.Subreddit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SubredditRepository extends JpaRepository<Subreddit, Integer> {
    Optional<Subreddit> findByName(String subredditName);
}
