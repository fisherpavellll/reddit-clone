package com.fisherpavel.redditclone.repositories;

import com.fisherpavel.redditclone.entities.Post;
import com.fisherpavel.redditclone.entities.User;
import com.fisherpavel.redditclone.entities.Vote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VoteRepository extends JpaRepository<Vote, Integer> {

    Optional<Vote> findTopByPostAndUserOrderByVoteIdDesc(Post post, User currentUser);
}
